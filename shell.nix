with import <nixpkgs> { }; let
  inherit (lib) optional optionals;

  elixir = beam.packages.erlangR24.elixir_1_13;
  oxalica_overlay = import (
    builtins.fetchTarball
    "https://github.com/oxalica/rust-overlay/archive/master.tar.gz"
  );
  nixpkgs = import <nixpkgs> { overlays = [ oxalica_overlay ]; };
  rust_channel = nixpkgs.rust-bin.stable."1.58.1".default;
in
  mkShell {
    buildInputs = [ elixir ];
    nativeBuildInputs =
      [
        asciidoctor
        cocogitto
        direnv
        elixir
        git
        llvmPackages.clang
        (rust_channel.override { extensions = [ "rust-src" "rust-std" ]; })
      ]
      ++ optional stdenv.isLinux inotify-tools
      ++ optionals stdenv.isDarwin
      (with darwin.apple_sdk.frameworks; [ CoreFoundation CoreServices ]);

    shellHook = ''
      cog install-hook all
      export SOURCE_DATE_EPOCH=$$('${git}/bin/git log -1 --pretty=%ct');
    '';

    ERL_INCLUDE_PATH = "${erlang}/lib/erlang/usr/include";
    LIBCLANG_PATH = "${pkgs.llvmPackages.libclang.lib}/lib";
    # Fix github:NixOS/nix#599
    LOCALE_ARCHIVE = "/usr/lib/locale/locale-archive";
  }
